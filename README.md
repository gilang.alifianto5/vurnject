# Vurnject

Vurnject adalah sebuah tools yang berfungsi untuk mencari vurnerability SQL Injection pada sebuah website.
Kita hanya perlu memasukan dorks pada file dorkslist.yml, maka tools akan melakukan scanning untuk mencari
vurn web menggunakan dorks tersebut.

# Requirements
- pip install requests
- pip install PyYAML
- pip install argparse
- pip install beautifulsoup4
- pip install selenium
- pip install PyVirtualDisplay

# Creator
- Author: Yusril Rapsanjani
- Language use: Python3
- Facebook: http://facebook.com/yuranitakeuchi

# Usage
- python3 vurnject.py -d dorkslist.yml -c [loadnumber]

# Screenshots
Tampilan utama Vurnject ketika melakukan proses scanning vurnerability
![](vurnject.png)

Tampilan Vurnject setelah berhasil mendapatkan dan menampilkan daftar website yang vurn
![](vurn.png)