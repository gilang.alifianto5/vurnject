from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pyvirtualdisplay import Display
import requests

'''
    @author: Yusril Rapsanjani
    @code ./yurani
    @site: www.yurani.me

'''

class HTTPReq:

    def __init__(self, url):
        self.url = url

    def getContent(self):
        display = Display(visible=0, size=(800, 600))
        display.start()

        driver = webdriver.Firefox()
        driver.get(self.url)
        content = driver.page_source
        driver.quit()

        display.stop()

        return content

    def getRequest(self):
        r = requests.get(self.url, timeout=10)
        #Karena hasil value berupa byte
        #maka kita harus melakukan decode ke utf-8
        #untuk mereturn value string
        req = r.content.decode("utf-8")
        return req